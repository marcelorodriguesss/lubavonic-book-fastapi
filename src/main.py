# Example 8-16
import uvicorn
from fastapi import FastAPI
from web import explorer, creature


app = FastAPI()

app.include_router(explorer.router)

app.include_router(creature.router)


@app.get("/")
def welcome():
    return "FastAPI!"


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True)
