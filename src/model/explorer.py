# Example 8-10
from pydantic import BaseModel


class Explorer(BaseModel):
    name: str
    country: str
    description: str
