import uvicorn
import asyncio
from fastapi import FastAPI


app = FastAPI()


@app.get("/hi")
async def greet():
    await asyncio.sleep(3)
    return "Hello? World?"


if __name__ == "__main__":
    uvicorn.run("example-3p15:app", reload=True)
